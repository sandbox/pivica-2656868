CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Website Page module... @todo

 * For a full description of the module, visit the project page:
   https://drupal.org/project/website_page

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/website_page

REQUIREMENTS
------------

This module requires the following modules:

 * none

RECOMMENDED MODULES
-------------------

 * Multiupload Filefield Widget
   (https://www.drupal.org/project/multiupload_filefield_widget)
   When enabled you can use Multiupload Filefield Widget for website page
   assets field for easier&faster files uploading.

 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.md help will be rendered
   with markdown.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - @todo

     @todo

   - @todo

     @todo

 * Check assets field configurations on admin/structure/types/manage/website-page/fields
   and change what is needed.
   
 * @todo - what else?

TROUBLESHOOTING
---------------

 * Nothing for now.

FAQ
---

Q: No questions for now?

A: Nope.
